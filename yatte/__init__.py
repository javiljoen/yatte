"""A simple task runner"""

from .tasklist import task  # noqa: F401

__version__ = "0.10.0"
